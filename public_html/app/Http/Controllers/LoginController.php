<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use \Input;
use \Validator;
use \Auth;
use \Redirect;
use \Config;

class LoginController extends Controller {

    public function login()
    {
        /* create rules to validate */
        $rules = array(
            'email' => 'required|email',
            'password' => 'required|alphaNum|min:3'
        );

        $messages = array(
            'email.required' => 'Musisz podać adres email.',
            'email.email' => 'Musisz podać prawidłowy adres email.',
            'password.required' => 'Musisz podać hasło.',
            'password.min' => 'Hasło nie może być krótsze niż :min znaków.',
            'password.alphaNum' => 'Hasło powinno się składać ze znaków alfanumerycznych.'
        );

        /* run the validator on the inputs from the form */
        $validator = Validator::make(Input::all(), $rules, $messages);

        /* failed validation */
        if($validator->fails()) {
            return Redirect::to('login')->withErrors($validator)->withInput(Input::except('password'));
        }else{
            /* now we can create userdata for auth */
            $userdata = array(
                'email' => strtolower(Input::get('email')),
                'password' => Input::get('password'),
                'active' => '1'
            );

            if(Auth::attempt($userdata)) {
                /* login success */
                return Redirect::intended( '/admin' )->with('notices', ['Zostałeś pomyślnie zalogowany.']);
            }else{
                /* login fail - wrong username or password or inactive account */
                return Redirect::to('login')->with('errors',['Logowanie nieudane.'])->withInput(Input::except('password'));
            }
        }
    } //end login()

    public function logout()
    {
        Auth::logout();
        return Redirect::to('login')->with('notices', ['Zostałeś wylogowany']);
    }

}
