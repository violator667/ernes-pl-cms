<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function()
{
    Route::get('/', 'AdminController@index');
});

//TODO: remove
//Route::get('/adduser', function() {
//    $user = new \App\User();
//    $user->name = 'normal user';
//    $user->email = 'michal@mil.net.pl';
//    $user->active = '1';
//    $user->password = bcrypt('qwerty666');
//    $user->save();
//});

//Route::get('/addpermisions', function() {
//    $service = new \App\Role();
//    $service->name =            'service';
//    $service->display_name =    'Ernes.pl Service';
//    $service->description   =   'Ernes.pl Service role';
//    $service->save();
//
//    $admin = new \App\Role();
//    $admin->name =              'admin';
//    $admin->display_name    =   'Administrator strony';
//    $admin->description     =   'Administrator: zezwala na dostęp do panelu administracyjnego.';
//    $admin->save();
//
//    $user = \App\User::find(1);
//    $user->attachRole($service); // parameter can be an Role object, array, or id
//
//    //permisions
//    $viewAdmin = new \App\Permission();
//    $viewAdmin->name = 'view-admin';
//    $viewAdmin->display_name = 'Dostęp do panelu administracyjnego';
//    $viewAdmin->description = 'Dostęp do panelu administracyjnego';
//    $viewAdmin->save();
//
//    $admin->attachPermission($viewAdmin);
//    $service->attachPermission($viewAdmin);
//
//});

Route::get('login', function() {
    return view('admin.login');
});
Route::post('login', 'LoginController@login');

Route::get('logout', 'LoginController@logout');

/* LAVAREL ROUTES */
Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
