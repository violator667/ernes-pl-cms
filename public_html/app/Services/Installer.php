<?php
/* 
 * ernes-pl CMS
 * 
 * Author: Michał Zielonka-Majka
 * violator667@gmail.com
 *
 * File: Installer.php
 *
 * Build: 2015-05-01
 *
 * Licence: Commercial - for details ask ernes.pl
 *
 */



namespace App\Services;

use \App\Install;

class Installer {

    /*
     * Returns admin main menu
     *
     */
    public function getAdminMenu()
    {
        $menu = Install::installed()->available();
        dd($menu);
    }
}