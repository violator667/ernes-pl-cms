<?php
/* 
 * ernes-pl CMS
 * 
 * Author: Michał Zielonka-Majka
 * violator667@gmail.com
 *
 * File: Permission.php
 *
 * Build: 2015-04-29
 *
 * Licence: Commercial - for details ask ernes.pl
 *
 */



namespace App;

use Zizaco\Entrust\EntrustPermission;
class Permission extends EntrustPermission {

}