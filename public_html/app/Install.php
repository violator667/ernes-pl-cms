<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Install extends Model {

	public function scopeInstalled($query)
    {
        return $query->where('installed', '1');
    }

    public function scopeAvailable($query)
    {
        return $query->where('available', '1');
    }

}
