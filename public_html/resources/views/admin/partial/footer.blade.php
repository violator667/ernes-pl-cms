<?php
/* 
 * ernes-pl CMS
 * 
 * Author: Michał Zielonka-Majka
 * violator667[at]gmail.com
 *
 * File: footer.blade.php
 *
 * Build: 2015-04-30
 *
 * Licence: Commercial - for details ask ernes.pl
 *
 */
?>
<!-- jQuery -->
<script src="{{ Config::get('app.url') }}/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ Config::get('app.url') }}/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{ Config::get('app.url') }}/bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="{{ Config::get('app.url') }}/bower_components/raphael/raphael-min.js"></script>
<script src="{{ Config::get('app.url') }}/bower_components/morrisjs/morris.min.js"></script>
<script src="{{ Config::get('app.url') }}/js/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="{{ Config::get('app.url') }}/dist/js/sb-admin-2.js"></script>
<script src="{{ Config::get('app.url') }}/cms/ernes.js"></script>

</body>

</html>