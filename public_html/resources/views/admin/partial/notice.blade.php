<?php
/* 
 * ernes-pl CMS
 * 
 * Author: Michał Zielonka-Majka
 * violator667[at]gmail.com
 *
 * File: notice.blade.php
 *
 * Build: 2015-04-30
 *
 * Licence: Commercial - for details ask ernes.pl
 *
 */
$notices = Session::get('notices');
?>
@if(count($notices) > 0)
    <div class="alert alert-success alert-dismissable">
        <strong>OK!</strong> Operacja zakończona powodzeniem<br><br>
        <ul>
            @if(is_object($notices))
                @foreach ($notices->all() as $notice)
                    <li>{{ $notice }}</li>
                @endforeach
            @elseif(is_array($notices))
                @foreach ($notices as $notice)
                    <li>{{ $notice }}</li>
                @endforeach
            @else
                <li>{{ $notice }}</li>
            @endif
        </ul>
    </div>
@endif