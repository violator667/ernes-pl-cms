<?php
/* 
 * ernes-pl CMS
 * 
 * Author: Michał Zielonka-Majka
 * violator667[at]gmail.com
 *
 * File: header.blade.php
 *
 * Build: 2015-04-30
 *
 * Licence: Commercial - for details ask ernes.pl
 *
 */
?>
<!DOCTYPE html>
<html lang="pl">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Ernes.pl CMS">
    <meta name="author" content="ernes.pl">

    <title>{{ Config::get('app.site_name') }}</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ Config::get('app.url') }}/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ Config::get('app.url') }}/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="{{ Config::get('app.url') }}/dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ Config::get('app.url') }}/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ Config::get('app.url') }}/bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ Config::get('app.url') }}/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>