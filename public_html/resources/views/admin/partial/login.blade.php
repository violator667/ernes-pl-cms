<?php
/* 
 * ernes-pl CMS
 * 
 * Author: Michał Zielonka-Majka
 * violator667[at]gmail.com
 *
 * File: login.blade.php
 *
 * Build: 2015-04-30
 *
 * Licence: Commercial - for details ask ernes.pl
 *
 */


?>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-body" style="text-align: center;">
                    <img src="cmsadmin/img/logo.png">
                </div>
                <div class="panel-body">
                    @include('admin.partial.error')

                        @include('admin.partial.notice')

                </div>
                <div class="panel-body">
                    <form role="form" method="POST" action="{{ url('/login') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="Adres E-mail" name="email" type="email" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Hasło" name="password" type="password" value="">
                            </div>
                            <!-- Change this to a button or input when using this as a form -->
                            <button type="submit" class="btn btn-lg btn-success btn-block">zaloguj</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>