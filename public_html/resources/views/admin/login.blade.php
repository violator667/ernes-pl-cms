<?php
/* 
 * ernes-pl CMS
 * 
 * Author: Michał Zielonka-Majka
 * violator667[at]gmail.com
 *
 * File: login.blade.php
 *
 * Build: 2015-04-30
 *
 * Licence: Commercial - for details ask ernes.pl
 *
 */

?>
@include('admin.partial.header')
@include('admin.partial.login')
@include('admin.partial.footer')