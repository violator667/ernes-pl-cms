<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstallsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('installs', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('module');
            $table->string('version');
            $table->integer('installed')->default('1');
            $table->integer('available')->default('1');
            $table->string('module_name');
            $table->string('admin_route');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('installs');
	}

}
